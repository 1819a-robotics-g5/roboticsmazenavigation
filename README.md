# Maze navigation Robotics Project #

## Team members: ##
 * Olga Stepanova
 * Ranno Lauri
 * Eka Rusadze
 * Aleksandra Panfilova
 
 
## Project details ##
### Overview ###
The maze will be designed with black lines against white background. There should be enough space in between the lines for the GoPiGo robot. The robot will navigate the maze without crossing the lines. It will choose the shortest path from any random starting position inside the maze. There will be a camera connected to a laptop that will observe the robot and the whole maze from above. The image processing will be done on the laptop which can then send the data to the robot, which will use the maze navigation algorithm to decide and follow the shortest path to the exit. Except for getting the image processing data from the laptop, the robot will be autonomous.

### Schedule for Pair A ###
#### Week 1: ####
* Finding maze images which will be used for image processing, attaching them to project repository (1h).
* Finding correct data scructure for image processing (10h).
* Starting to implement image processing (using data structure from previous point) (10h).

#### Week 2: ####
* Continue working on image processing (20h).
* Start working on robot navigation based on video feedback (together with team B) (20h).

#### Week 3: ####
* Combining software and hardware, debugging and making additional fixes together with team B (10h).

#### Week 4: ####
* Making poster for presentation (15h).

#### Week 5:####
* Poster session with live demo together with pair B.

### Schedule for Pair B ###
#### Week 1: ####
* Find suitable algorithm for finding shortest path in maze and implement it (10h).
* Implement communication between pi and laptop (sockets?) (30h) 
 
#### Week 2: ####
* Implement calculation of shortest path algorithm using data structure received from team A (30h).
* Make physical maze (only if we don't get it from University, 5h) 
* Start working on robot navigation based on video feedback (together with team A) (20h).

#### Week 3: ####
* Combining software and hardware, debugging and making additional fixes together with team A (10h).
* Make video for demo (5h).

#### Week 4:####
* Making additional debugging and fixes (5h).

#### Week 5:####
* Poster session with live demo together with pair A.

### Component list ###
| Item                                        | Quantity    | whether we-need-this-component-from-you |
| --------------------------------------------|-------------|-----------------------------------------|
| GoPiGo                                      | 1           | yes                                     |
| Battery                                     | 1           | yes                                     |
| SD-card                                     | 1           | yes                                     |
| Pi                                          | 1           | yes                                     |
| Pi Power supply                             | 1           | yes                                     |
| Camera                                      | 1           | yes                                     |
| Laptops                                     | 4           | no                                      |
| Maze                                        | 1           | yes                                     |
| Tripod or anything to hold camera           | 1           | yes                                     |
| Bright-color paper for GoPiGo color-coding  | 1           | no                                      |

### Challenges and solutions ###
| Challenge                                   | Solution          |
| ------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------ |
| Image prosessing: analysing the maze        | Dividing an image into a grid of robot's size; analysing sectors and giving them values of: 2 for an exit, 1 for a pass, 0 for an obstacle |
| Data format for transmission to Pi          | Matrix of 0, 1, 2                                                                                                                          |
| Indication of robot's orientation           | Colour coding                                                                                                                              |
| Finding the shortest way to the exit        | A* search algorithm                                                                                                                        |
| Data transmission from laptop to Pi         | Socket                                                                                                                                     |