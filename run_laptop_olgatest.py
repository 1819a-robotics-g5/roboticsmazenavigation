import cv2 as cv
import numpy as np
import cv2 as cv2
import socket
import pickle
import struct
import time
from Marker import *
import AlgorithmTest
from math import atan2, degrees
import _thread

#global for holding Marker
MARKER = None

#global for holding Robot coordinates
ROBOT_X = 0
ROBOT_Y = 0

#turn to True to use live video stream
VIDEO = True

#turn to True to connecto to Pi
CONNECTION = False

#camera reso
ORIGINAL_FRAME_WIDTH = 960
ORIGINAL_FRAME_HEIGHT = 720

#amount of pixels next to ArUco marker which should be counted as part of robot
PI_AREA_X = 35
PI_AREA_Y = 55


#TODO adjust according to floor
IMG_LOWER_LIMITS = [0, 0, 0]
IMG_UPPER_LIMITS = [200, 120, 50]

THRESHOLD_LOWER = 127
THRESHOLD_UPPER = 255

IMG_RESIZE_ROWS = int(ORIGINAL_FRAME_WIDTH / 10) #10 worked somehow
IMG_RESIZE_COLUMNS = int(ORIGINAL_FRAME_HEIGHT / 10)


#magical numbers are count of rows and culumns in matrix
MAZE_RESO_W = int(IMG_RESIZE_ROWS / (IMG_RESIZE_ROWS / 4))
MAZE_RESO_H = int(IMG_RESIZE_COLUMNS / (IMG_RESIZE_COLUMNS / 4))

#TODO: IP of pi, change according to your network
host = '192.168.43.225'
port = 11000

sock = None


#set to 0 if you don't have camera on laptop
if VIDEO == True:
    cap = cv2.VideoCapture(1)
    cap.set(11, 70)  # contrast       min: 0   , max: 255 , increment:1
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, ORIGINAL_FRAME_WIDTH)
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, ORIGINAL_FRAME_HEIGHT)
    cap.set(cv2.CAP_PROP_FPS, 1)
dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_5X5_250)


def detectMarkers(frame):
    # This function detects all the markers from the current frame
    # The returned structure is a list, where:
    # First element (index 0) is a tuple of ([list of marker corner coordinates (as lists first element)], [type of variable the coordinates are represented in])
    # Second element is list of marker id numbers, each as a list.
    # the lists are in same order (when using coordinate list like detected[0][0][0][0][0], then this is the first coordinate of marker with id detected[1][0][0])


    detected = cv2.aruco.detectMarkers(frame, dictionary)
    # Convert the complicated structure used by ArUco into a more convenient form. See Marker.py
    markers = parseMarkers(detected)

    return markers

# robot_front - list of float: coordinates of the marker corner towards the front of the robot
def get_angle(marker):
    robot_front = marker.corners[0]
    robot_back = marker.corners[2]

    robot_center = ((robot_front[0] + robot_back[0]) / 2, (robot_front[1] + robot_back[1]) / 2)  # find the center coordinate pair of the robot

    x1 = robot_front[0] - robot_back[0]  # Robot vector x
    y1 = robot_front[1] - robot_back[1]  # Robot vector y

    x2 = int(np.mean(MARKER.x)) # From robot center to next destination x
    y2 = int(np.mean(MARKER.y))  # From robot center to next destination y

    dot = x1 * x2 + y1 * y2  # dot product
    det = x1 * y2 - y1 * x2  # determinant
    angle = degrees(atan2(det, dot))  # atan2(y, x) or atan2(sin, cos)

    return angle

def get_matrix(img, original):
    global MARKER
    global ROBOT_X
    global ROBOT_Y
    # get count of grids rows and columns
    image_data = np.asarray(img)
    rows = int(np.ceil(len(image_data) / MAZE_RESO_H))
    columns = int(np.ceil(len(image_data[0]) / MAZE_RESO_W))

    markers = detectMarkers(original)
    if len(markers) > 0:
        MARKER = markers[0]
        cv2.circle(original, (int(np.mean(MARKER.x)), int(np.mean(MARKER.y))), 50, (0, 255, 0), -1)

        #cv2.circle(original, (int(MARKER.corners[2][0]), int(MARKER.corners[2][1])), 10, (0, 255, 0), -1)
        cv2.imshow('Result', original)


    #resize in same way as for maze processing
    original = cv2.resize(original, (IMG_RESIZE_ROWS, IMG_RESIZE_COLUMNS))
    lowerLimits = np.array([0, 255, 0])
    upperLimits = np.array([0, 255, 0])
    robot_img = cv2.inRange(original, lowerLimits, upperLimits)
    cv2.imshow('Robot small', robot_img)

    matrix = []
    matrix_row_counter = 0
    matrix_col_counter = 0
    for i in range(rows):
        matrixrow = []
        for j in range(columns):
            # 1 = WHITE, 0 - BLACK
            matrixcol = 1
            for pr in range(i * MAZE_RESO_H, (i * MAZE_RESO_H + MAZE_RESO_H) - 1):
                if pr > len(image_data) - 1 or matrixcol == 0:
                    break
                for pk in range(j * MAZE_RESO_W, (j * MAZE_RESO_W + MAZE_RESO_W) - 1):
                    if pk > len(image_data[1]) - 1:
                        break
                    #part of robot marked as 3
                    if robot_img[pr][pk] != 0:
                        ROBOT_Y = matrix_col_counter
                        ROBOT_X = matrix_row_counter
                        matrixcol = 3
                        break
                    if image_data[pr][pk] < 200:
                        matrixcol = 0
                        break
            matrixrow.append(matrixcol)
            matrix_col_counter += 1
        matrix.append(matrixrow)
        matrix_col_counter = 0
        matrix_row_counter += 1


    '''
    #Minimalize matrix according to maze structure

    #detect rectangle, not sure if needed
    #find horizontal maze boundaries:
    horizontal_sums = []
    for x in range(len(matrix)):
        horizontal_sums.append(sum(matrix[x]))

    #horizontal boundaries
    top_horizontal = np.argmin(horizontal_sums)
    horizontal_sums.pop(top_horizontal)
    down_horizontal = np.argmin(horizontal_sums) + 1


    '''

    matrix = markExit(matrix)

    #That saves result to file just for debugging, file actually not needed
    with open('maze_matrix.txt', 'w') as f:
        for item in matrix:
            item = ''.join(map(str, item))
            item = item.replace('\r', '').replace('\n', '').replace(' ', '')
            f.write("%s\n" % item)

    return matrix


def markExit(matrix):
    exitFound = False
    npMatrix = np.array(matrix)
    exitColumn1 = npMatrix[:, [0]]
    exitColumn2 = npMatrix[:, [len(npMatrix[0]) - 1]]
    for x in range(len(exitColumn1)):
        for y in range(len(exitColumn1[x])):
            if exitColumn1[x][y] == 1 and (exitFound == False or matrix[x-1][y] == 2):
                matrix[x][y] = 2
                exitFound = True

    if exitFound:
        return matrix

    # exit not found in first matrix column - check last one
    for x in range(len(exitColumn2)):
        for y in range(len(exitColumn2[x])):
            if exitColumn2[x][y] == 1 and (exitFound == False or matrix[x-1][y] == 2):
                matrix[x + len(npMatrix[0]) - 1][y] = 2
                exitFound = True

    if exitFound:
        return matrix

    # Not needed for our size because we have vertical exit, but can be useful is maze is swapped
    # exit still not found, check first row
    for x in range(matrix[0]):
        if matrix[0][x] == 1:
            matrix[0][x] = 2
            exitFound = True
    if exitFound:
        return matrix

    for x in range(matrix[len(matrix) - 1]):
        if matrix[0][x] == 1:
            matrix[0][x] = 2

    return matrix

def get_robot_location(img):
    #blob detection for robot etc
    #TODO should return x and y coordinates of robot based on location on matrix
    return ['3', '1']


def send_data_to_pi(data, wait_for_reply=False):
    global sock
    print('Sending to pi' + str(data))
    data = pickle.dumps(data)
    length = len(data)
    sock.sendall(struct.pack('!I', length))

    sock.sendall(data)
    try:
        if wait_for_reply:
            reply = sock.recv(1024)
            print('Received response from pi')
            return True
    except:
        return False


def start_server():
    global sock
    global host
    global port
    try:
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((host, port))
        print('Connected to pi')
    except socket.error as msg:
        print('Connection to pi failed')


def get_img():
    global ORIGINAL_TO_PROCESSED_PIXEL_RATIO_W
    global ORIGINAL_TO_PROCESSED_PIXEL_RATIO_H
    global IMG_LOWER_LIMITS
    global IMG_UPPER_LIMITS
    global THRESHOLD_LOWER
    global THRESHOLD_UPPER
    global VIDEO

    #TODO: in real life we should use video only, others are for debugging
    if VIDEO == False:
        img = cv.imread("maze_photo_other_direction.jpg")
    else:
        ret, img = cap.read()
        '''
        while True:
            cv2.imshow('img', img)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        '''
        #height, width, channels = img.shape
        #print(height)
        #print(width)

    lowerLimits = np.array(IMG_LOWER_LIMITS)
    upperLimits = np.array(IMG_UPPER_LIMITS)
    processed = cv2.inRange(img, lowerLimits, upperLimits)
    _, processed = cv.threshold(processed, THRESHOLD_LOWER, THRESHOLD_UPPER, cv2.THRESH_BINARY_INV)

    processed = cv2.resize(processed, (IMG_RESIZE_ROWS, IMG_RESIZE_COLUMNS))
    cv2.imshow('Thresholded', processed)

    return [processed, img]


started = False
startedThread = False

def piThread():
    while running:
        angle1 = get_angle(MARKER)
        send_data_to_pi([angle1, ROBOT_X, ROBOT_Y], True)



#skip first frames after camera opening, there are laggings when is just starts to work
i = 0
#sending robot coordinates to pi
start_server()
while True:
    images = get_img()
    matrix = get_matrix(images[0], images[1])
    if i > 20:
        if CONNECTION:
            if started == False:
                started = send_data_to_pi(matrix, True)
            elif startedThread == False:
                startedThread = True
                _thread.start_new_thread(piThread, ())  # Start the second thread.
    i = i + 1
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break