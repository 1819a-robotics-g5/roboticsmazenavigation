import socket as s
import numpy as np
import struct
import pickle
import heapq
import gopigo as go
from math import atan2
from math import pi
import time
import AlgorithmTest

go.set_speed(100)

#should be true when robot is ready to navigate
started = False

connection = None


def start_server():
    global connection
    sock = s.socket(s.AF_INET, s.SOCK_STREAM)
    host = '0.0.0.0'
    port = 11000
    sock.bind((host, port))
    sock.listen(5)

    #Listens for incoming requests from laptop
    connection, addr = sock.accept()
    print('SERVER  started')


#functions for receiving data via sockets without cutting-off (up to 4GB)
#check http://stupidpythonideas.blogspot.com/2013/05/sockets-are-byte-streams-not-message.htmlh
def recv_message():
    global connection
    lengthbuf = recvall(4)
    length, = struct.unpack('!I', lengthbuf)
    return recvall(length)


def recvall(count):
    global connection
    try:
        buf = b''
        while count:
            newbuf = connection.recv(count)
            if not newbuf:
                return None
            buf += newbuf
            count -= len(newbuf)
        return buf
    except:
        print('Socket reading error appeared')

def robot_navigation(path):
    global data, robot_angle
    for a in range(len(path)):
        vectori = path[a+1][0] - path[a][0]
        vectorj = path[a+1][1] - path[a][1]
        theta = atan2(vectorj, vectori)
        thetadegrees = theta * 180 / pi
        if thetadegrees < 0:
            thetadegrees = 360 + thetadegrees
        beta = 180 + robot_angle
        degreedifference = thetadegrees - beta
        duration = abs(0.5 * degreedifference / 180)
        if degreedifference < -50:
            go.right_rot()
            time.sleep(duration)
            #turn right
        elif degreedifference > 50:
            go.left_rot()
            time.sleep(duration)
            #turn left
        else:
            go.fwd()
            time.sleep(duration+0.1)
        robot_angle = data[0]
        go.stop()
    return 0

start_server()
while True:
    data = recv_message()
    print(data)
    data = pickle.loads(data)
    if started == False:
        data = np.asarray(data)
        print('Received maze')
        print(data)
        #maze_solving(data)
        #thecell = Cell()
        astar = AlgorithmTest.AStar()
        astar.init_grid(data)
        thepath = astar.solve()
        #Notify laptop that maze was solved and now it should track robot moving
        connection.send('STARTED'.encode())
        started = True
    else:
        print('Received robot coordinates')
        robot_angle = data[0]
        robot_x = data[1]
        robot_y = data[2]
        robot_navigation(thepath)
        connection.send('PROCESSED'.encode())
        started = True
sock.close()
