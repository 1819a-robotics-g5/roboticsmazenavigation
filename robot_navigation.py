if robot_angle in range(-pi/2, 0):
    beta = robot_angle - pi/2
elif robot_angle in range(-pi, -pi/2):
    beta = 3*pi/2 + robot_angle
elif robot_angle in range(pi/2, pi):
    beta = robot_angle - pi/2
elif robot_angle in range(0, pi/2):
    beta = robot_angle - pi/2