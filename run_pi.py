import socket as s
import numpy as np
import struct
import pickle
import heapq
import gopigo as go
from math import atan2
from math import pi
import time
import AlgorithmTest

go.set_speed(70)

#should be true when robot is ready to navigate
started = False

connection = None


def start_server():
    global connection
    sock = s.socket(s.AF_INET, s.SOCK_STREAM)
    host = '0.0.0.0'
    port = 15000
    sock.bind((host, port))
    sock.listen(5)

    #Listens for incoming requests from laptop
    connection, addr = sock.accept()
    print('SERVER  started')


#functions for receiving data via sockets without cutting-off (up to 4GB)
#check http://stupidpythonideas.blogspot.com/2013/05/sockets-are-byte-streams-not-message.htmlh
def recv_message():
    global connection
    lengthbuf = recvall(4)
    length, = struct.unpack('!I', lengthbuf)
    return recvall(length)


def recvall(count):
    global connection
    try:
        buf = b''
        while count:
            newbuf = connection.recv(count)
            if not newbuf:
                return None
            buf += newbuf
            count -= len(newbuf)
        return buf
    except:
        print('Socket reading error appeared')

def robot_navigation(path):
    global data, robot_angle, robot_x, robot_y

    for a in range(len(path)-1):
        vectori = path[a+1][0] - path[a][0]
        vectorj = -(path[a+1][1] - path[a][1])
        point_x = path[a][0]
        point_y = path[a][1]
        #if abs(robot_x - point_x) <= 2 and abs(robot_y - point_y) <=2:

        theta = atan2(vectorj, vectori)
        # robot_angle = robot_angle * pi /180
        robot_angle = round(robot_angle)
        if robot_angle in range(0, -90, -1):
            beta = robot_angle - 90
        elif robot_angle in range(-90, -180, -1):
            beta = 270 + robot_angle
        elif robot_angle in range(90, 180):
            beta = robot_angle - 90
        elif robot_angle in range(0, 90):
            beta = robot_angle - 90
        thetadegrees = theta * 180 / pi
        # if thetadegrees < 0:
        #    thetadegrees = 360 + thetadegrees
        # beta = 180 + robot_angle
        if beta > 0:
            degreedifference = thetadegrees - beta
            duration = abs(0.2 * degreedifference / 180)
            print('beta', beta, 'degreedifference', degreedifference)
            if degreedifference < -45:
                go.right_rot()
                time.sleep(duration)
            elif degreedifference > 45:
                go.left_rot()
                time.sleep(duration)
            else:
                go.fwd()
                time.sleep(duration + 0.2)
        else:
            degreedifference = thetadegrees - beta
            duration = abs(0.2 * degreedifference / 180)
            print('beta', beta, 'degreedifference', degreedifference)
            if degreedifference > 45:
                go.left_rot()
                time.sleep(duration)
            elif degreedifference < -45:
                go.right_rot()
                time.sleep(duration)
            else:
                go.fwd()
                time.sleep(duration + 0.2)
        go.stop()
            # degreedifference = thetadegrees - beta
            # duration = abs(0.5 * degreedifference / 180)
            # if degreedifference < -50:
            #     go.right_rot()
            #     time.sleep(duration)
            #     #turn right
            # elif degreedifference > 50:
            #     go.left_rot()
            #     time.sleep(duration)
            #     #turn left
            # else:
            #     go.fwd()
            #     time.sleep(duration+0.1)
        # else:
        #     if a == 0:
        #         a = 1
        #     a = a -1
        #     go.stop()
        robot_angle = data[0]
        robot_x = data[1]
        robot_y = data[2]
        go.stop()

    return 0

start_server()
while True:
    data = recv_message()
    print(data)
    data = pickle.loads(data)
    if started == False:
        data = np.asarray(data)
        print('Received maze')
        print(data)
        #maze_solving(data)
        #thecell = Cell()
        astar = AlgorithmTest.AStar()
        astar.init_grid(data)
        thepath = astar.solve()
        #Notify laptop that maze was solved and now it should track robot moving
        connection.send('STARTED'.encode())
        started = True
    else:
        print('Received robot coordinates')
        robot_angle = data[0]
        robot_x = data[1]
        robot_y = data[2]
        robot_navigation(thepath)
        connection.send('PROCESSED'.encode())
        started = True
sock.close()
